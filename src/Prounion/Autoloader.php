<?php
function prounion_ip_autoload($classname)
{
    $filename = str_replace("\\", DIRECTORY_SEPARATOR, $classname).".php";
    
    if (file_exists(dirname(__FILE__) . '/../' . $filename)) {
        require_once (dirname(__FILE__) . '/../' . $filename);
    }
}

spl_autoload_register('prounion_ip_autoload');